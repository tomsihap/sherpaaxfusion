import React, { Component, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import * as FS from "expo-document-picker";
import Web3 from 'web3'
import Web3FusionExtend from 'web3-fusion-extend'
import styles from "./Styles";
import { Images } from "../../Themes";
import Balance from '../Balance'
type Props = {};
type State = {
  privateKey: string,
	isShowBalance: boolean,
	balances: any
};
class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      privateKey: "",
		isShowBalance: false,
		balances: null
    };
  }
  onOpenFile = () => {
    FS.getDocumentAsync({
      type: "*/*",
      copyToCacheDirectory: true
    });
  };
  render() {
  	const {isShowBalance, balances} = this.state
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={Images.logo} />
        <Text style={styles.title}>Access Existing Wallet</Text>
	
	      	<KeyboardAvoidingView behavior={"position"} style={styles.form}>
		      <Text style={styles.textKey}>Private Key: </Text>
		      <View style={styles.inputCover}>
			      <TextInput
				      autoCapitalize={"none"}
				      autoCorrect={false}
				      autoFocus={false}
				      onChangeText={privateKey => {
					      this.setState({privateKey});
				      }}
				      style={styles.textInput}
				      placeholder={"Enter your private key"}
			      />
		      </View>

		      <TouchableOpacity
			      // onPress={this.onOpenFile}
			      onPress={() => this.props.navigation.navigate('WalletAssetInfo')}
			      style={styles.buttonCover}
		      >
			      <Text style={styles.button}>Open The Wallet</Text>
		      </TouchableOpacity>
		      <TouchableOpacity
			      // onPress={this.onOpenFile}
			      onPress={() => this.setState({
				      isShowBalance: true
			      })}
			      style={styles.buttonCover}
		      >
			      <Text style={styles.button}>See the Balance</Text>
		      </TouchableOpacity>
	      </KeyboardAvoidingView>
      </View>
    );
  }
}

export default Home;
