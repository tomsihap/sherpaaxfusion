import React, { useEffect } from 'react'
import {
	View,
	Text,
	Image,
	TextInput,
	KeyboardAvoidingView,
	TouchableOpacity
} from 'react-native'
import * as FS from 'expo-document-picker'
import Web3 from 'web3'
import Web3FusionExtend from 'web3-fusion-extend'
import '../../Config/global'
import styles from '../HomeScreen/Styles'

type Props = {
	balance: string
};
type State = {
	privateKey: string,
	balance: string
};

const provider = new Web3.providers.WebsocketProvider(
	'wss://testnetpublicgateway1.fusionnetwork.io:10001'
)

provider.on('data', () => {
	console.log('connected')
})

provider.on('error', () => {
	console.log('error')
})

const web3 = new Web3(provider)
const web3FusionExtend = Web3FusionExtend.extend(web3)

function Balance(props) {
	useEffect(() => {
		web3.eth
		.getBlock(200)
		.then(block => {
			console.log(block)
		})
		.catch(err => {
			console.log('error', err)
		})
	}, [])

	function getAllBalances() {
		web3FusionExtend.fsn
		.getAllBalances('0xC4A9441afB052cB454240136CCe71Fb09316EA94')
		.then(balances => {
			const {balance} = props
			balance(balances)
		})
		.catch(err => {
			console.log('error', err)
		})
	}
	return (
		<View style={styles.container}>
			<Text>Open up App.tsx to start working on your app!</Text>
			<TouchableOpacity
				// onPress={this.onOpenFile}
				onPress={props.goBack}
				style={[styles.buttonCover, {alignSelf: 'center'}]}
			>
				<Text style={styles.button}>Go back</Text>
			</TouchableOpacity>
			<TouchableOpacity
				// onPress={this.onOpenFile}
				onPress={getAllBalances}
				style={[styles.buttonCover, {alignSelf: 'center'}]}
			>
				<Text style={styles.button}>Get all balance</Text>
			</TouchableOpacity>
			<Text>{JSON.stringify(props.balances)}</Text>
		</View>
	)
}

export default Balance
