import { StyleSheet } from "react-native";
import { Metrics } from "../../../Themes";
import metrics from "../../../Themes/Metrics";
export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50
  },
  logo: {
    width: (metrics.screenWidth * 40) / 100,
    height: (metrics.screenWidth * 10) / 100
  },
  title: {
    fontSize: (metrics.screenWidth * 6) / 100,
    textDecorationLine: "underline",
    color: "#7B7B7B",
    alignSelf: "center",
    marginVertical: (metrics.screenWidth * 6) / 100
  },
  form: {
    marginTop: (metrics.screenWidth * 10) / 100,
    justifyContent: "center",
    alignItems: "center"
  },
  textKey: {
    fontSize: (metrics.screenWidth * 5) / 100,
    marginVertical: (metrics.screenWidth * 5) / 100
  },
  inputCover: {
    borderWidth: 1,
    width: (metrics.screenWidth * 85) / 100,
    borderColor: "#7B7B7B"
  },
  textInput: {
    width: (metrics.screenWidth * 85) / 100,
    height: (metrics.screenWidth * 12) / 100,

    textAlign: "center"
  },
  buttonCover: {
    width: (metrics.screenWidth * 85) / 100,
    height: (metrics.screenWidth * 12) / 100,
    backgroundColor: "#81AFE0",
    justifyContent: "center",
    alignItems: "center",
    marginVertical : 10
  },
  button: {
    color: '#ffffff'
  }
});
