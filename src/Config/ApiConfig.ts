const DOMAIN = 'http://api.fusionnetwork.io/'
export default {
	baseURL: DOMAIN,
	headers: {
		'Content-Type': 'application/json',
		'Cache-Control': 'no-cache'
	},
	timeOut: 30000
}
