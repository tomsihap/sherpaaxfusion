import { createAppContainer, createStackNavigator } from "react-navigation";
import Home from "../../Screen/HomeScreen/index";
import WalletAssetInfo from "../../Screen/WalletAssetInfo";
const MainStack = createStackNavigator({
  Home: {
    screen: Home
  },
	WalletAssetInfo: {
	  screen: WalletAssetInfo
  }},{
	initialRouteName: 'Home',
	headerMode: 'none'
});

export default createAppContainer(MainStack);
