import { createStore, applyMiddleware } from "redux";
import Reducer from "./index";
import rootSaga from "../Sagas";

import createSagaMiddleware from "redux-saga";
const sagaMiddleWare = createSagaMiddleware();
const CreateStore = createStore(Reducer, applyMiddleware(sagaMiddleWare));
export default CreateStore;
sagaMiddleWare.run(rootSaga);
