import { takeEvery } from "redux-saga/effects";
import { FirstActions } from "../Redux/FirstReducer";

function* first() {
  console.log("take every first action");
}
export function* f() {
  yield takeEvery(FirstActions, first);
}
