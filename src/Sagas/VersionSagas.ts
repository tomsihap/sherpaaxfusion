import { call, put, takeEvery } from 'redux-saga/effects'
import { checkVersionRequest } from '../Redux/VersionReducer'
import VersionApi from '../Services/VersionApi'
function* VersionCheck(action: any) {
	const versionApi = VersionApi.create()
	try {
		const { version } = action
		const response = yield call(versionApi.checkVersion, version)
		if (response.ok && response.status === 200) {
			yield put({type: "USER_FETCH_SUCCEEDED", user: response} )
		}
	} catch (e) {
		console.log(e)
		yield put({type: "CHECK_VERSION_FAILURE", message: e.message})
	}
}

export function* check() {
	yield takeEvery(checkVersionRequest, VersionCheck);
}
