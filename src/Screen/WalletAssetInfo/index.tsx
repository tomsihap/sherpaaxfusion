import React, { Component } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import { WalletInfoInput } from './components'
import { Images, Metrics } from '../../Themes'
import styles from './Styles'

type Props = {};

class WalletAssetInfo extends Component<Props> {
	constructor(props: Props) {
		super(props)
	}

	render() {
		return (
      <KeyboardAvoidingView behavior={'position'} style={styles.form}>
				<Image resizeMode={'contain'} source={Images.logo} style={styles.logo}/>
				<Text style={styles.title}>
					Wallet Info
				</Text>
				<View style={styles.wrapBalance}>
					<Text style={styles.category}>
						Fusion Balance:
					</Text>
					<View style={styles.infoBalance}>
						<Text style={styles.numberBalance}>
							50
						</Text>
						<Text style={[styles.numberBalance, {fontSize: 20, marginBottom: 5, marginLeft: 4}]}>
							FSN
						</Text>
					</View>
					<Text style={styles.category}>
						Public Address:
					</Text>
					<Text style={styles.publicKey}>
						0xC4A9441afB052cB454240136CCe71Fb09316EA94
					</Text>
				</View>
				<View style={styles.feature}>
					<Text style={styles.textFeatureName}>
						Asset Creation
					</Text>
					<View style={styles.wrapInfo}>
						<WalletInfoInput
							name={'Asset Name :'}
							input={''}
						/>
						<WalletInfoInput
							name={'Supply :'}
							input={''}
						/>
						<TouchableOpacity style={styles.featureButton}>
							<Text style={styles.titleButton}>
								Create Asset
							</Text>
						</TouchableOpacity>
					</View>
				</View>
				<View style={styles.feature}>
					<Text style={styles.textFeatureName}>
						Sent Asset
					</Text>
					<View style={styles.wrapInfo}>
						<WalletInfoInput
							name={'To :'}
							input={''}
						/>
						<WalletInfoInput
							name={'Quantily :'}
							input={''}
						/>
						<TouchableOpacity style={styles.featureButton}>
							<Text style={styles.titleButton}>
								Send Asset
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</KeyboardAvoidingView>
		)
	}
}

export default WalletAssetInfo
