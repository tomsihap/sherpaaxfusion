
export const FirstActions = "FIRST_ACTIONS";

const initState = 0;
export default function(state = initState, action) {
  switch (action.type) {
    case FirstActions:
      console.log(action);
      return state + 1;
    default:
      return state;
  }
}
