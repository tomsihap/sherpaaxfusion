import React from "react";
import { Provider } from "react-redux";
import RootStack from "./src/Navigation/index";
import CreateStore from "./src/Redux/CreateStore";

export default function App() {
  return (
    <Provider store={CreateStore}>
      <RootStack />
    </Provider>
  );
}
