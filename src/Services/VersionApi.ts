import apisauce from 'apisauce'
import ApiConfig from '../Config/ApiConfig'

const create = (baseURL = (ApiConfig.baseURL + '/version')) => {
	const api = apisauce.create({
		baseURL,
		headers: ApiConfig.headers,
		timeout: ApiConfig.timeOut
	})

	function checkVersion (version) {
		return api.post(`/check`, {
			version
		})
	}

	return {
		checkVersion
	}
}

export default {
	create
}
