export const checkVersionRequest = "CHECK_VERSION_REQUEST";
export const checkVersionSuccess = "CHECK_VERSION_SUCCESS";
export const checkVersionFailure = "CHECK_VERSION_FAILURE";
const initState = 0;
export default function(state = initState, action) {
	switch (action.type) {
		case checkVersionRequest:
			console.log(action);
			return state + 1;
		case checkVersionSuccess:
			console.log(action);
			return 0;
		case checkVersionFailure:
			console.log(action);
			return 1;
	}
}
