import { StyleSheet } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { Metrics } from '../../Themes'
import metrics from '../../Themes/Metrics'

const styles = StyleSheet.create({
	form: {
		marginHorizontal: Metrics.medium,
		marginBottom: Metrics.base,
		justifyContent: "center",
		alignItems: "center"
	},
	logo: {
		...ifIphoneX({
			width: Metrics.screenWidth / 3,
			height: Metrics.screenWidth / 4
		}, {
			width: Metrics.screenWidth / 3,
			height: Metrics.screenWidth / 5
		}),
		position: 'absolute',
		top: Metrics.base,
		marginLeft: -Metrics.medium,

	},
	title: {
		marginTop: Metrics.screenHeight / 10,
		alignSelf: 'center',
		textDecorationLine: 'underline',
		fontWeight: '700',
		fontSize: 25
	},
	wrapBalance: {
		flex: 1,
		justifyContent: 'center'
	},
	category: {
		fontSize: Metrics.doubleBase
	},
	infoBalance: {
		...ifIphoneX({
			marginVertical: Metrics.base
		},{}),
		flexDirection: 'row',
		alignItems: 'flex-end'
	},
	numberBalance: {
		fontWeight: '700',
		fontSize: 40
	},
	publicKey: {
		fontWeight: '700',
		fontSize: 14
	},
	feature: {
		flex: 1,
		marginHorizontal: Metrics.doubleBase
	},
	textFeatureName: {
		fontSize: 25,
		fontWeight: '700',
		textDecorationLine: 'underline'
	},
	assetInfo: {
		marginTop: Metrics.base,
		flexDirection: 'row',
		alignItems: 'center'
	},
	wrapInfo: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	textAssetCategory: {
		flex: 1,
		fontWeight: '700',
		fontSize: 18
	},
	inputAsset: {
		flex: 1,
		paddingLeft: Metrics.base,
		borderWidth: 1,
		paddingVertical: Metrics.base
	},
	featureButton: {
		marginTop: Metrics.base / 2,
		alignSelf: 'flex-end',
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleButton: {
		fontWeight: '600',
		backgroundColor: "#81AFE0",
		color: 'white',
		paddingHorizontal: Metrics.doubleBase,
		paddingVertical: 3
	},
});
export default styles