import { all } from "redux-saga/effects";
import { f } from "./FirstSaga";
import { check } from "./VersionSagas";
export default function* rootSaga() {
  yield all([
	  f(),
	  check()
  ]);
}
