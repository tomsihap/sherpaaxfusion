import { Dimensions, Platform } from 'react-native'
const {width, height} = Dimensions.get('window')

const metrics = {
	base: 10,
	medium: 16,
	doubleBase: 20,
	margin_edge_medium: 17,
	tripleBase: 30,
	quadrupleBase: 40,
	quintupleBase: 50,
	image_default_size: 60,
	screenWidth: width < height ? width : height,
	screenHeight: width < height ? height : width
}

export default metrics
