import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import styles from '../Styles'

type Props = {
	name: string,
	input: string
};

class WalletInfoInput extends Component<Props> {
	constructor(props: Props) {
		super(props)
	}

	render() {
		const {name, input} = this.props
		return (
			<View style={styles.assetInfo}>
				<Text style={styles.textAssetCategory}>
					{name}
				</Text>
				<TextInput style={styles.inputAsset}/>
			</View>
		)
	}
}

export default WalletInfoInput
